README
======

This repository contains [Artix Linux](https://artixlinux.org) PKGBUILD files which
require aarch64-specific changes and some scripts simplifying
work. Most of the PKGBUILDs are taken from [ALARM](https://github.com/archlinuxarm/PKGBUILDs) repository.

The following directory structure is supposed:


*	wd\
*	*	artix\
*	*	*	all cloned Artix Linux repos
*	*	artixarm
*	*	packages
*	*	PKGBUILDs

PKGBUILDs repo is only used for recieving changes from ALARM.
buildpkg-arm script requires artools to be installed. It
searches for PKGBUILD in artixarm first, then in artix

artixarm repo also contains patched atrools.
packages directory is a place where all built packages will be
copied. It is structured like usual [Artix packages repos](http://mirror1.artixlinux.org/repos/)
without testing ang staging repos.
